Name:           libtaskotron
Version:        0.4.18
Release:        1%{?dist}
Summary:        Taskotron Support Library

License:        GPLv3
URL:            https://pagure.io/taskotron/libtaskotron
Source0:        https://qa.fedoraproject.org/releases/%{name}/%{name}-%{version}.tar.gz

BuildArch:      noarch

# sorted alphabetically
Requires:       libtaskotron-config = %{version}-%{release}
Requires:       libtaskotron-core = %{version}-%{release}
Requires:       libtaskotron-fedora = %{version}-%{release}
Requires:       libtaskotron-disposable = %{version}-%{release}

%description
Libtaskotron is a support library for running taskotron tasks. The top level
libtaskotron package is a meta-package which will install all sub-packages.

%package -n libtaskotron-core
Summary:        The minimal, core parts of libtaskotron

Requires:       libtaskotron-config = %{version}-%{release}
Requires:       python-configparser >= 3.5.0b2
Requires:       python-jinja2 >= 2.8
Requires:       python-progressbar >= 2.3
Requires:       python-resultsdb_api >= 1.3.0
Requires:       python-requests >= 2.7.0
Requires:       python-setuptools
Requires:       python-xunitparser >= 1.3.3
Requires:       PyYAML >= 3.11
Requires:       sudo
Requires:       tar >= 1.28-6
BuildRequires:  pytest >= 2.7.3
BuildRequires:  python2-devel
BuildRequires:  python-configparser >= 3.5.0b2
BuildRequires:  python-dingus >= 0.3.4
BuildRequires:  python-mock >= 2.0.0
BuildRequires:  python-progressbar >= 2.3
BuildRequires:  python-resultsdb_api >= 1.3.0
BuildRequires:  python-requests >= 2.7.0
BuildRequires:  python-setuptools
BuildRequires:  python-xunitparser >= 1.3.3
BuildRequires:  PyYAML >= 3.11

%description -n libtaskotron-core
The minimal, core parts of libtaskotron that are needed to run tasks

%package -n libtaskotron-config
Summary:        Configuration files needed for using libtaskotron

%description -n libtaskotron-config
libtaskotron-config contains all of the configuration files needed for using
libtaskotron.

%package -n libtaskotron-fedora
Summary:        Fedora specific module for libtaskotron

Requires:       createrepo
Requires:       dnf >= 0.6.4
Requires:       koji
Requires:       libtaskotron-core = %{version}-%{release}
Requires:       mash
Requires:       python-fedora >= 0.8.0
Requires:       python-hawkey >= 0.4.13-1
Requires:       python-munch >= 2.0.2
Requires:       rpm-python
BuildRequires:  koji
BuildRequires:  mash
BuildRequires:  python-fedora >= 0.8.0
BuildRequires:  python-hawkey >= 0.4.13-1
BuildRequires:  python-munch >= 2.0.2

%description -n libtaskotron-fedora
Module for libtaskotron which contains all functionality which is fedora-only

%package -n libtaskotron-disposable
Summary:        disposable client module for libtaskotron

Requires:       libtaskotron-core = %{version}-%{release}
Requires:       python-paramiko >= 1.15.1
Requires:       testcloud >= 0.1.10
BuildRequires:  python-paramiko >= 1.15.1
BuildRequires:  testcloud >= 0.1.10

%description -n libtaskotron-disposable
Module for libtaskotron which enables the use of disposable clients


%pre core
getent group taskotron >/dev/null || groupadd taskotron

%prep
%setup -q

%check
%{__python} setup.py test

%build
%{__python} setup.py build

%install
rm -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root %{buildroot}

# configuration files
mkdir -p %{buildroot}%{_sysconfdir}/taskotron/
install -m 0644 conf/taskotron.yaml.example %{buildroot}%{_sysconfdir}/taskotron/taskotron.yaml
install -m 0644 conf/namespaces.yaml.example %{buildroot}%{_sysconfdir}/taskotron/namespaces.yaml
install -m 0644 conf/yumrepoinfo.conf.example %{buildroot}%{_sysconfdir}/taskotron/yumrepoinfo.conf

# log dir
install -d %{buildroot}/%{_localstatedir}/log/taskotron

# tmp dir
install -d %{buildroot}/%{_tmppath}/taskotron

# artifacts dir
install -d %{buildroot}/%{_sharedstatedir}/taskotron/artifacts

# cache dir
install -d %{buildroot}/%{_localstatedir}/cache/taskotron

# images dir
install -d %{buildroot}/%{_sharedstatedir}/taskotron/images

%files

%files -n libtaskotron-core
%doc README.rst
%license LICENSE
%{python2_sitelib}/libtaskotron/*.py*
%{python2_sitelib}/libtaskotron/directives/*.py*
%{python2_sitelib}/libtaskotron/ext/*.py*
%{python2_sitelib}/libtaskotron/report_templates/*.j2
%{python2_sitelib}/*.egg-info

%dir %{python2_sitelib}/libtaskotron/ext/fedora
%dir %{python2_sitelib}/libtaskotron/ext
%dir %{python2_sitelib}/libtaskotron/directives
%dir %{python2_sitelib}/libtaskotron/report_templates

%attr(755, root, root) %{_bindir}/runtask
%attr(755, root, root) %{_bindir}/taskotron_result

%dir %attr(2775, root, taskotron) %{_tmppath}/taskotron
%dir %attr(2775, root, taskotron) %{_localstatedir}/log/taskotron
%dir %attr(2775, root, taskotron) %{_localstatedir}/cache/taskotron
%dir %attr(2775, root, taskotron) %{_sharedstatedir}/taskotron
%dir %attr(2775, root, taskotron) %{_sharedstatedir}/taskotron/artifacts
%dir %attr(2775, root, taskotron) %{_sharedstatedir}/taskotron/images

%files -n libtaskotron-config
%dir %{_sysconfdir}/taskotron
%config(noreplace) %{_sysconfdir}/taskotron/taskotron.yaml
%config(noreplace) %{_sysconfdir}/taskotron/namespaces.yaml
%config %{_sysconfdir}/taskotron/yumrepoinfo.conf

%files -n libtaskotron-fedora
%dir %{python2_sitelib}/libtaskotron/ext/fedora
%{python2_sitelib}/libtaskotron/ext/fedora/*

%files -n libtaskotron-disposable
%dir %{python2_sitelib}/libtaskotron/ext/disposable
%{python2_sitelib}/libtaskotron/ext/disposable/*

%changelog
* Wed Jan 11 2017 Martin Krizek <mkrizek@fedoraproject.org> - 0.4.18-1
- distgit directive improvements (D1055, D1069)
- yumrepoinfo.conf updates (D1057, D1074)
- koji_utils: use supported arches when downloading build logs (D1076)

* Thu Nov 3 2016 Tim Flink <tflink@fedoraproject.org> - 0.4.17-1
- add support for resultsdb 2.0 (D1019)
- add armhfp as a primary arch (D1040)

* Wed Aug 17 2016 Martin Krizek <mkrizek@fedoraproject.org> - 0.4.16-1
- symlink testcloud images instead of copying
- copy dir to minion via tarball
- yumrepoinfo.conf: Fedora 25 has been branched

* Thu Jul 28 2016 Martin Krizek <mkrizek@fedoraproject.org> - 0.4.15-1
- Allow resultsdb_directive to read from file
- create_report_directive: allow input from file
- bash to shell directive
- yumrepoinfo.conf: F22 is now EOL
- various fixes and improvements

* Tue Jul 12 2016 Martin Krizek <mkrizek@fedoraproject.org> - 0.4.14-1
- add xunit directive
- docker testing documentation
- taskotron_result tool
- various fixes and improvements

* Fri May 27 2016 Martin Krizek <mkrizek@fedoraproject.org> - 0.4.13-3
- fix download URL for Source0
- fix conf files permissions

* Thu May 26 2016 Martin Krizek <mkrizek@fedoraproject.org> - 0.4.13-2
- remove not needed custom python_sitelib macro

* Thu May 26 2016 Martin Krizek <mkrizek@fedoraproject.org> - 0.4.13-1
- add distgit docs
- fix progressbar
- koji: allow to exclude arches
- remote_exec: update to latest packages even when cache is outdated

* Tue May 10 2016 Martin Krizek <mkrizek@fedoraproject.org> - 0.4.12-2
- remove global write permissions of libtaskotron directories and change group ownership
  to the taskotron group

* Mon May 9 2016 Martin Krizek <mkrizek@fedoraproject.org> - 0.4.12-1
- bash directive
- allow using a dot to access attributes of a variable in formula
- various fixes

* Mon Apr 18 2016 Martin Krizek <mkrizek@fedoraproject.org> - 0.4.11-1
- add result namespace whitelist
- various fixes and small improvements

* Tue Mar 15 2016 Martin Krizek <mkrizek@fedoraproject.org> - 0.4.10-3
- add python-configparser as dep
- install namespaces.yaml

* Mon Mar 14 2016 Martin Krizek <mkrizek@fedoraproject.org> - 0.4.10-2
- add report_templates directory

* Thu Mar 03 2016 Martin Krizek <mkrizek@fedoraproject.org> - 0.4.10-1
- various performance fixes in disposable clients

* Sun Feb 07 2016 Tim Flink <tflink@fedoraproject.org> - 0.4.9-1
- add report directive (D735)
- restructure docs (D734)

* Thu Jan 28 2016 Martin Krizek <mkrizek@redhat.com> - 0.4.8-2
- Add jinja2 as a dep

* Mon Jan 25 2016 Tim Flink <tflink@fedoraproject.org> - 0.4.8-1
- Documentation updates (D726, D725)

* Wed Jan 20 2016 Tim Flink <tflink@fedoraproject.org> - 0.4.7-1
- changed filename structure for disposable images (D696)
- fixed crash when a check does not provide result (D723)
- Lots of documentation fixes and updates

* Wed Dec 9 2015 Martin Krizek <mkrizek@redhat.com> - 0.4.6-1
- urlgrabber replaced with requests (D668)
- Make minion repos configurable (D682)
- Copy all config files on a minion (D662)
- Fix module import error masking missing modules (D659)

* Fri Nov 20 2015 Martin Krizek <mkrizek@redhat.com> - 0.4.5-2
- Require the exact same version of subpackages
- Remove unused deps

* Thu Nov 19 2015 Martin Krizek <mkrizek@redhat.com> - 0.4.5-1
- Do not fail when copying a task from cwd (D656)
- We need -core not -disposable on disposable clients (D652)
- Don't require -disposable in -core (D652)

* Thu Nov 12 2015 Martin Krizek <mkrizek@redhat.com> - 0.4.4-1
- add %files for libtaskotron meta-package so it's actually built
- make imageurl configurable
- change package install commands to work with new subpackage structure

* Thu Nov 12 2015 Tim Flink <tflink@fedoraproject.org> - 0.4.3-1
- break libtaskotron up into subpackages (D616)

* Tue Nov 3 2015 Martin Krizek <mkrizek@redhat.com> - 0.4.2-1
- setup.py: fix entry point
- check if packages are installed before installing them
- taskotron.yaml.example: fix typo

* Tue Nov 3 2015 Martin Krizek <mkrizek@redhat.com> - 0.4.1-1
- Save tmp files into /tmpdir/$username (D632)
- find latest image using filename convention (D618)
- yumrepoinfo.conf: Fedora 23 has been released (D633)

* Wed Oct 14 2015 Martin Krizek <mkrizek@redhat.com> - 0.4.0-1
- merge disposable branch to add option to execute the task in a throwaway client

* Wed Sep 02 2015 Josef Skladanka <jskaladan@fedoraproject.org> - 0.3.24-1
- remove pytap, bayeux and yamlish dependencies

* Fri Aug 28 2015 Tim Flink <tflink@fedoraproject.org> - 0.3.23-1
- only submit bodhi comments on failure (D542)

* Wed Aug 26 2015 Tim Flink <tflink@fedoraproject.org> - 0.3.22-1
- Re-enabling bodhi comment directive (D527)
- Include artifacts dir at end of run (D533)

* Wed Aug 19 2015 Tim Flink <tflink@fedoraproject.org> - 0.3.21-1
- Fixing bad initialization of bodhi_utils (D514)
- Removing default value of None for fas_password (D511)

* Tue Aug 18 2015 Tim Flink <tflink@fedoraproject.org> - 0.3.20-1
- Fixing tests to not try network connecitons (T574)

* Tue Aug 18 2015 Tim Flink <tflink@fedoraproject.org> - 0.3.19-1
- Fixes to make libtaskotron mostly compatible with bodhi2 (T558)

* Wed Jul 15 2015 Tim Flink <tflink@fedoraproject.org> - 0.3.17-1
- several minor fixes and enhancements, see git log for more information

* Wed Jul 8 2015 Martin Krizek <mkrizek@redhat.com> - 0.3.16-1
- 0.3.16 release. See git log for more information

* Wed May 13 2015 Kamil Paral <kparal@redhat.com> - 0.3.15-2
- synchronize package versions between spec file and requirements.txt (D337)

* Mon Apr 20 2015 Tim Flink <tflink@fedoraproject.org> - 0.3.15-1
- Do not report ABORTED runs to bodhi (T467)

* Mon Apr 20 2015 Tim Flink <tflink@fedoraproject.org> - 0.3.14-1
- Removing logrotate in production (D339)
- Adding status to bodhi operations and removing excess queries (D344)

* Tue Apr 7 2015 Kamil Paral <kparal@redhat.com> - 0.3.13-2
- Require python-setuptools, otherwise runtask fails to execute. See T449.

* Tue Mar 31 2015 Martin Krizek <mkrizek@redhat.com> - 0.3.13-1
- Add support for execdb
- Various fixes

* Wed Feb 25 2015 Martin Krizek <mkrizek@redhat.com> - 0.3.12-1
- Add support for task artifacts

* Thu Feb 12 2015 Tim Flink <tflink@fedoraproject.org> - 0.3.11-1
- Adding 'compose' item type (T381)
- Fix issue with mash when no RPMs downloaded (T351)

* Wed Oct 22 2014 Tim Flink <tflink@fedoraproject.org> - 0.3.10-1
- Fix for i386 unit tests (T361)
- Small documentation fixes

* Fri Oct 17 2014 Martin Krizek <mkrizek@redhat.com> - 0.3.9-1
- Improve logging messages
- Update documentation

* Thu Oct 9 2014 Tim Flink <tflink@fedoraproject.org> - 0.3.8-1
- Adding bodhi query retries for read-only operations (T338)
- several small bugfixes and typo corrections

* Fri Aug 22 2014 Tim Flink <tflink@fedoraproject.org> - 0.3.7-1
- Adding mash as a BR for functional tests
- removing all as an option for runtask

* Fri Aug 22 2014 Martin Krizek <mkrizek@redhat.com> - 0.3.6-1
- Releasing libtaskotron 0.3.6

* Tue Jul 08 2014 Martin Krizek <mkrizek@fedoraproject.org> - 0.3.3-2
- Add /var/log/taskotron directory

* Mon Jun 30 2014 Tim Flink <tflink@fedoraproject.org> - 0.3.3-1
- Changed distibution license to gpl3
- New user-facing docs

* Mon Jun 23 2014 Tim Flink <tflink@fedoraproject.org> - 0.3.2-1
- Gracefully handle missing rpms in build. Fixes T251.

* Mon Jun 23 2014 Tim Flink <tflink@fedoraproject.org> - 0.3.1-1
- Better support for depcheck in koji_utils and mash_directive
- Added ability for check name to be specified in TAP13 output

* Mon Jun 16 2014 Tim Flink <tflink@fedoraproject.org> - 0.3.0-1
- Added sphinx to requirements.txt

* Fri Jun 13 2014 Tim Flink <tflink@fedoraproject.org> - 0.2.1-1
- documentation improvements, added LICENSE file
- better support for depcheck, srpm downloading
- improved logging configuration

* Wed May 28 2014 Tim Flink <tflink@fedoraproject.org> - 0.1.1-1
- adding libtaskotron-config as requires for libtaskotron
- changing variable syntax to $var and ${var}
- add yumrepoinfo directive, other bugfixes

* Fri May 16 2014 Tim Flink <tflink@fedoraproject.org> - 0.1.0-1
- Releasing libtaskotron 0.1

* Fri May 09 2014 Tim Flink <tflink@fedoraproject.org> - 0.0.11-4
- Disabling %check so that the code can be used while the tests are fixed

* Tue May 06 2014 Kamil Páral <kparal@redhat.com> - 0.0.11-3
- Add a minimum version to python-hawkey. Older versions have a different API.

* Tue Apr 29 2014 Tim Flink <tflink@fedoraproject.org> - 0.0.11-2
- moved config files to libtaskotron-config subpackage

* Tue Apr 29 2014 Tim Flink <tflink@fedoraproject.org> - 0.0.11-1
- changed config files to be noreplace
- updated config file source paths

* Tue Apr 29 2014 Tim Flink <tflink@fedoraproject.org> - 0.0.10-1
- yumrepo fix for upgradepath, output fix for readability

* Fri Apr 25 2014 Tim Flink <tflink@fedoraproject.org> - 0.0.9-1
- koji_utils fix for upgradepath

* Fri Apr 25 2014 Tim Flink <tflink@fedoraproject.org> - 0.0.8-1
- Updating to latest upstream version
- Fixing resultsdb integration, adding some rpm and koji utility methods

* Wed Apr 16 2014 Tim Flink <tflink@fedoraproject.org> - 0.0.7-2
- Fixing some urls and other small packaging changes

* Tue Apr 15 2014 Tim Flink <tflink@fedoraproject.org> - 0.0.7-1
- Updating to latest upstream
- Change to more generic CLI arguments to match reporting and work better with buildbot

* Mon Apr 14 2014 Tim Flink <tflink@fedoraproject.org> - 0.0.6-2
- Initial package for libtaskotron
