# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

import pytest
import StringIO
from dingus import Dingus

from libtaskotron.directives import yumrepoinfo_directive
from libtaskotron.exceptions import TaskotronDirectiveError

from libtaskotron.ext.fedora import yumrepoinfo


TEST_CONF = '''\
[DEFAULT]
baseurl = http://download.fedoraproject.org/pub/fedora/linux
goldurl = %(baseurl)s/releases/%(path)s/Everything/%(arch)s/os
updatesurl = %(baseurl)s/updates/%(path)s/%(arch)s
rawhideurl = %(baseurl)s/%(path)s/%(arch)s/os
arches = i386, x86_64
parent =
tag = %(__name__)s
supported = no

[rawhide]
path = development/rawhide
url = %(rawhideurl)s
tag = f21

[f20]
url = %(goldurl)s
path = 20
supported = yes

[f20-updates]
url = %(updatesurl)s
path = 20
parent = f20
arches = x86_64

[f20-updates-testing]
url = %(updatesurl)s
path = testing/20
parent = f20-updates
'''


class TestYumrepoinfoDirective(object):

    @classmethod
    @pytest.fixture(autouse=True)
    def setup_class(cls, tmpdir):
        '''One-time class initialization'''
        # create YumRepoInfo initialized with TEST_CONF
        cls.temp_conf = tmpdir.join("functest_yumrepoinfo.conf")
        cls.temp_conf.write(TEST_CONF)

    def test_missing_kojitag(self):
        directive = yumrepoinfo_directive.YumrepoinfoDirective(filelist=[self.temp_conf.strpath])
        ref_input = {"arch": ["x86_64"]}

        with pytest.raises(TaskotronDirectiveError):
            directive.process(ref_input, None)

    def test_missing_arch(self):
        directive = yumrepoinfo_directive.YumrepoinfoDirective(filelist=[self.temp_conf.strpath])
        ref_input = {"koji_tag": "rawhide"}

        with pytest.raises(TaskotronDirectiveError):
            directive.process(ref_input, None)

    def test_pending(self):
        directive = yumrepoinfo_directive.YumrepoinfoDirective(filelist=[self.temp_conf.strpath])
        ref_input = {"koji_tag": "f20-pending", "arch": ["x86_64"]}

        output = directive.process(ref_input, None)

        assert output == {"f20": {"x86_64": "http://download.fedoraproject.org/pub/fedora/linux/releases/20/Everything/x86_64/os"}}

    def test_rawhide(self):
        directive = yumrepoinfo_directive.YumrepoinfoDirective(filelist=[self.temp_conf.strpath])
        ref_input = {"koji_tag": "rawhide", "arch": ["x86_64"]}

        output = directive.process(ref_input, None)

        assert output == {"rawhide": {"x86_64": "http://download.fedoraproject.org/pub/fedora/linux/development/rawhide/x86_64/os"}}

    def test_bad_kojitag(self):
        directive = yumrepoinfo_directive.YumrepoinfoDirective(filelist=[self.temp_conf.strpath])
        ref_input = {"koji_tag": "my random tag"}

        with pytest.raises(TaskotronDirectiveError):
            directive.process(ref_input, None)

    def test_repo_path(self):

        directive = yumrepoinfo_directive.YumrepoinfoDirective(filelist=[self.temp_conf.strpath])
        ref_input = {"koji_tag": "f20-updates", "arch": ["x86_64"]}

        output = directive.process(ref_input, None)

        assert output == {
            "f20": {"x86_64": "http://download.fedoraproject.org/pub/fedora/linux/releases/20/Everything/x86_64/os"},
            "f20-updates": {"x86_64": "http://download.fedoraproject.org/pub/fedora/linux/updates/20/x86_64"},
            }

    def test_use_arch(self, monkeypatch):
        """Make sure that the arch passed in as an arg is used to create the
        yumrepoinfo object instead of falling back to the default system arch"""
        ref_arch = 'i386'
        repoinfo = yumrepoinfo.YumRepoInfo(filelist=[], arch=['x86_64'])
        repoinfo.parser.readfp(StringIO.StringIO(TEST_CONF))

        stub_getrepoinfo = Dingus(return_value=repoinfo)
        monkeypatch.setattr(yumrepoinfo, 'get_yumrepoinfo', stub_getrepoinfo)

        # don't set the repoinfo object, we've stubbed out the code that would
        # hit the filesystem, so it's not a risk here
        directive = yumrepoinfo_directive.YumrepoinfoDirective()
        ref_input = {"koji_tag": "f20-updates", "arch": [ref_arch]}

        directive.process(ref_input, None)

        # check the first arg of the first call to the stub object
        assert stub_getrepoinfo.calls()[0][1][0] == ref_arch

    def test_only_meta_arches(self):
        directive = yumrepoinfo_directive.YumrepoinfoDirective(filelist=[self.temp_conf.strpath])
        ref_input = {"koji_tag": "f20", "arch": ["src", "noarch"]}

        with pytest.raises(TaskotronDirectiveError):
            directive.process(ref_input, None)

    def test_one_base_arch(self):
        directive = yumrepoinfo_directive.YumrepoinfoDirective(filelist=[self.temp_conf.strpath])
        ref_input = {"koji_tag": "f20", "arch": ["x86_64"]}

        output = directive.process(ref_input, None)

        assert output == {
            "f20": {"x86_64": "http://download.fedoraproject.org/pub/fedora/linux/releases/20/Everything/x86_64/os"}
            }

    def test_base_meta_arch(self):
        directive = yumrepoinfo_directive.YumrepoinfoDirective(filelist=[self.temp_conf.strpath])
        ref_input = {"koji_tag": "f20", "arch": ["src", "noarch", "x86_64"]}

        output = directive.process(ref_input, None)

        assert output == {
            "f20": {"x86_64": "http://download.fedoraproject.org/pub/fedora/linux/releases/20/Everything/x86_64/os"}
            }

    def test_multiple_base_arches(self):
        directive = yumrepoinfo_directive.YumrepoinfoDirective(filelist=[self.temp_conf.strpath])
        ref_input = {"koji_tag": "f20", "arch": ["i386", "x86_64"]}

        output = directive.process(ref_input, None)

        assert output == {
            "f20": {
                "i386": "http://download.fedoraproject.org/pub/fedora/linux/releases/20/Everything/i386/os",
                "x86_64": "http://download.fedoraproject.org/pub/fedora/linux/releases/20/Everything/x86_64/os"}
            }

    def test_multiple_base_meta_arches(self):
        directive = yumrepoinfo_directive.YumrepoinfoDirective(filelist=[self.temp_conf.strpath])
        ref_input = {"koji_tag": "f20", "arch": ["src", "i386", "x86_64"]}

        output = directive.process(ref_input, None)

        assert output == {
            "f20": {
                "i386": "http://download.fedoraproject.org/pub/fedora/linux/releases/20/Everything/i386/os",
                "x86_64": "http://download.fedoraproject.org/pub/fedora/linux/releases/20/Everything/x86_64/os"}
            }
