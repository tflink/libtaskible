# -*- coding: utf-8 -*-
# Copyright 2009-2016, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

from libtaskotron.directives import shell_directive
from libtaskotron.exceptions import TaskotronDirectiveError
import libtaskotron.os_utils

import os
import pytest
import mock

OUTPUT = "Happy panda"
PATH = os.path.realpath(__file__)


@pytest.mark.usefixtures("setup")
class TestCheckCommand(object):

    @pytest.fixture
    def setup(self, monkeypatch):
        self.shell_dir = shell_directive.ShellDirective()
        self.mock_run = mock.Mock()
        monkeypatch.setattr(libtaskotron.os_utils, 'popen_rt', self.mock_run)
        self.arg_data = {'task': PATH}

    def test_a_list(self):
        test_params = ['cmd.exe']
        self.mock_run.return_value = (OUTPUT, '')

        output = self.shell_dir.process(test_params, self.arg_data)

        assert output == OUTPUT

    def test_not_a_list(self, monkeypatch):
        test_params = {'a dict': 'instead of a list'}

        with pytest.raises(TaskotronDirectiveError):
            self.shell_dir.process(test_params, self.arg_data)

        assert not self.mock_run.called

    def test_cmd_not_a_str(self, monkeypatch):
        test_params = [False]

        with pytest.raises(TaskotronDirectiveError):
            self.shell_dir.process(test_params, self.arg_data)

        assert not self.mock_run.called

    def test_cmd_not_a_list_of_strs(self, monkeypatch):
        test_params = [['echo', False]]

        with pytest.raises(TaskotronDirectiveError):
            self.shell_dir.process(test_params, self.arg_data)

        assert not self.mock_run.called
