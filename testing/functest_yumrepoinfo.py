# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

'''Functional tests for libtaskotron/yumrepoinfo.py'''

import pytest

from libtaskotron.ext.fedora import yumrepoinfo
from libtaskotron import exceptions as exc

class TestYumRepoInfo(object):

    def test_init_filelist(self, tmpdir):
        '''test YumRepoInfo(filelist=) with custom config files'''
        conf1 = tmpdir.join('conf1.cfg')
        conf2 = tmpdir.join('conf2.cfg')
        conf1.write('''
[rawhide]
a = foo''')
        conf2.write('''
[rawhide]
b = bar''')
        repoinfo = yumrepoinfo.YumRepoInfo(filelist=[conf1.strpath,
                                                     conf2.strpath])

        # must read conf1
        assert repoinfo.get('rawhide', 'a') == 'foo'

        # must not read conf2
        with pytest.raises(exc.TaskotronConfigError):
            repoinfo.get('rawhide', 'b')

        # must not read default configs
        with pytest.raises(exc.TaskotronConfigError):
            repoinfo.get('rawhide', 'tag')

    def test_init_raise(self, tmpdir):
        '''Raise an exception when configs don't exist or are empty'''
        conf1 = tmpdir.join('conf1.cfg')
        conf2 = tmpdir.join('conf2.cfg')
        conf2.write('')

        # non-existent
        with pytest.raises(exc.TaskotronConfigError):
            yumrepoinfo.YumRepoInfo(filelist=[conf1.strpath])

        # empty
        with pytest.raises(exc.TaskotronConfigError):
            yumrepoinfo.YumRepoInfo(filelist=[conf2.strpath])
