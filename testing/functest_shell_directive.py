# -*- coding: utf-8 -*-
# Copyright 2009-2016, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

from libtaskotron.directives import shell_directive
from libtaskotron.exceptions import TaskotronDirectiveError

import os
import pytest

OUTPUT = "Happy panda"
PATH = os.path.realpath(__file__)


class TestCheckCommand(object):

    def test_existing_command(self):
        test_params = ['echo -n %s' % OUTPUT]
        test_arg_data = {'task': PATH}
        test_directive = shell_directive.ShellDirective()

        output = test_directive.process(test_params, test_arg_data)

        assert output == OUTPUT

    def test_shell_false(self):
        test_params = [['echo', '-n', OUTPUT]]
        test_arg_data = {'task': PATH}
        test_directive = shell_directive.ShellDirective()

        output = test_directive.process(test_params, test_arg_data)

        assert output == OUTPUT

    def test_non_existing_command(self):
        test_params = ['if_this_exists_i_will_go_to_hell_and_i_will_take_kparal_with_me']
        test_arg_data = {'task': PATH}
        test_directive = shell_directive.ShellDirective()

        with pytest.raises(TaskotronDirectiveError):
            test_directive.process(test_params, test_arg_data)

    def test_zero_returncode(self):
        test_params = ['true']
        test_arg_data = {'task': PATH}
        test_directive = shell_directive.ShellDirective()

        output = test_directive.process(test_params, test_arg_data)

        assert output == ""

    def test_nonzero_returncode(self):
        test_params = ['false']
        test_arg_data = {'task': PATH}
        test_directive = shell_directive.ShellDirective()

        with pytest.raises(TaskotronDirectiveError):
            test_directive.process(test_params, test_arg_data)

    def test_multi_command(self):
        test_params = ['true',
                       'echo -n %s' % OUTPUT[::-1],
                       'echo -n %s' % OUTPUT]
        test_arg_data = {'task': PATH}
        test_directive = shell_directive.ShellDirective()

        output = test_directive.process(test_params, test_arg_data)

        assert output == OUTPUT

    def test_multi_command_error(self):
        test_params = ['true',
                       'echo -n %s' % OUTPUT[::-1],
                       'false',
                       'echo -n %s' % OUTPUT]
        test_arg_data = {'task': PATH}
        test_directive = shell_directive.ShellDirective()

        with pytest.raises(TaskotronDirectiveError):
            test_directive.process(test_params, test_arg_data)

    def test_multi_command_ignore_error(self):
        test_params = ['true',
                       'echo -n %s' % OUTPUT[::-1],
                       {'ignorereturn': ['false']},
                       'echo -n %s' % OUTPUT]
        test_arg_data = {'task': PATH}
        test_directive = shell_directive.ShellDirective()

        output = test_directive.process(test_params, test_arg_data)

        assert output == OUTPUT
