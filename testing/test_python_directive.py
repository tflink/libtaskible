# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

from libtaskotron.directives import python_directive
from libtaskotron.exceptions import TaskotronDirectiveError
from dingus import Dingus
import os
import imp
import pytest
import copy


class TestCheckfile(object):
    def test_existing_file(self, monkeypatch):
        ref_filename = 'turbo_encabulator.py'

        stub_ospath = Dingus(exists__returns=True)

        test_directive = python_directive.PythonDirective()
        monkeypatch.setattr(os, 'path', stub_ospath)

        test_directive.checkfile(ref_filename)

    def test_not_existing_file(self, monkeypatch):
        ref_filename = 'turbo_encabulator.py'

        stub_ospath = Dingus(exists__returns=False)

        test_directive = python_directive.PythonDirective()
        monkeypatch.setattr(os, 'path', stub_ospath)

        with pytest.raises(TaskotronDirectiveError):
            test_directive.checkfile(ref_filename)


class TestLoadPyfile(object):
    def test_task_importname(self, monkeypatch):
        ref_filename = '/real/magic/turbo_encabulator.py'
        ref_taskname = 'runtask_turbo_encabulator'
        stub_imp = Dingus()

        test_directive = python_directive.PythonDirective()
        monkeypatch.setattr(imp, 'load_source', stub_imp)

        test_name, test_task = test_directive.load_pyfile(ref_filename)

        assert test_name == ref_taskname


class TestExecutePyfile(object):

    def test_task_method_is_found(self, monkeypatch):
        ref_modulename = 'foo'
        ref_methodname = 'bar'
        ref_kwargs = {'baz': 'why', 'blah': 2}

        stub_getattr = Dingus(return_value=lambda baz, blah: "str")
        stub_module = Dingus(__file__=ref_modulename)

        test_directive = python_directive.PythonDirective()
        monkeypatch.setattr(test_directive, '_do_getattr', stub_getattr)

        test_directive.execute(stub_module, ref_methodname, ref_kwargs)

        assert (stub_getattr.calls()[0].name, stub_getattr.calls()[0].args) == (
            '()', (stub_module, ref_methodname))

    def test_task_method_is_executed(self, monkeypatch):
        ref_modulename = 'foo'
        ref_methodname = 'bar'
        ref_kwargs = {'baz': 'why', 'blah': 2}

        stub_task = Dingus(return_value="str")
        stub_getattr = Dingus(return_value=stub_task)
        stub_module = Dingus(__file__=ref_modulename)

        test_directive = python_directive.PythonDirective()
        monkeypatch.setattr(test_directive, '_do_getattr', stub_getattr)

        test_directive.execute(stub_module, ref_methodname, ref_kwargs)

        assert (stub_task.calls()[0].name, stub_task.calls()[0].args,
                stub_task.calls()[0].kwargs) == ('()', (), ref_kwargs)


class TestPyfileOutputException(object):

    def test_task_doesnt_rise_on_unicode_str(self, monkeypatch):
        ref_modulename = 'foo'
        ref_methodname = 'bar'
        ref_kwargs = {'baz': 'why', 'blah': 2}

        stub_task = Dingus(return_value=u"unicode")
        stub_getattr = Dingus(return_value=stub_task)
        stub_module = Dingus(__file__=ref_modulename)

        test_directive = python_directive.PythonDirective()
        monkeypatch.setattr(test_directive, '_do_getattr', stub_getattr)

        test_directive.execute(stub_module, ref_methodname, ref_kwargs)

        assert (stub_task.calls()[0].name, stub_task.calls()[0].args,
                stub_task.calls()[0].kwargs) == ('()', (), ref_kwargs)

    def test_task_doesnt_rise_on_empty_str(self, monkeypatch):
        ref_modulename = 'foo'
        ref_methodname = 'bar'
        ref_kwargs = {'baz': 'why', 'blah': 2}

        stub_task = Dingus(return_value="")
        stub_getattr = Dingus(return_value=stub_task)
        stub_module = Dingus(__file__=ref_modulename)

        test_directive = python_directive.PythonDirective()
        monkeypatch.setattr(test_directive, '_do_getattr', stub_getattr)

        test_directive.execute(stub_module, ref_methodname, ref_kwargs)

        assert (stub_task.calls()[0].name, stub_task.calls()[0].args,
                stub_task.calls()[0].kwargs) == ('()', (), ref_kwargs)

    def test_task_doesnt_rise_on_none(self, monkeypatch):
        ref_modulename = 'foo'
        ref_methodname = 'bar'
        ref_kwargs = {'baz': 'why', 'blah': 2}

        stub_task = Dingus(return_value=None)
        stub_getattr = Dingus(return_value=stub_task)
        stub_module = Dingus(__file__=ref_modulename)

        test_directive = python_directive.PythonDirective()
        monkeypatch.setattr(test_directive, '_do_getattr', stub_getattr)

        test_directive.execute(stub_module, ref_methodname, ref_kwargs)

        assert (stub_task.calls()[0].name, stub_task.calls()[0].args,
                stub_task.calls()[0].kwargs) == ('()', (), ref_kwargs)

    def test_task_doesnt_rise_on_list(self, monkeypatch):
        ref_modulename = 'foo'
        ref_methodname = 'bar'
        ref_kwargs = {'baz': 'why', 'blah': 2}

        stub_task = Dingus(return_value=[])
        stub_getattr = Dingus(return_value=stub_task)
        stub_module = Dingus(__file__=ref_modulename)

        test_directive = python_directive.PythonDirective()
        monkeypatch.setattr(test_directive, '_do_getattr', stub_getattr)

        test_directive.execute(stub_module, ref_methodname, ref_kwargs)

        assert (stub_task.calls()[0].name, stub_task.calls()[0].args,
                stub_task.calls()[0].kwargs) == ('()', (), ref_kwargs)

    def test_task_doesnt_rise_on_dict_type(self, monkeypatch):
        ref_modulename = 'foo'
        ref_methodname = 'bar'
        ref_kwargs = {'baz': 'why', 'blah': 2}

        stub_task = Dingus(return_value={'han': 'shot', 'first': True})
        stub_getattr = Dingus(return_value=stub_task)
        stub_module = Dingus(__file__=ref_modulename)

        test_directive = python_directive.PythonDirective()
        monkeypatch.setattr(test_directive, '_do_getattr', stub_getattr)

        test_directive.execute(stub_module, ref_methodname, ref_kwargs)

        assert (stub_task.calls()[0].name, stub_task.calls()[0].args,
                stub_task.calls()[0].kwargs) == ('()', (), ref_kwargs)


class TestProcess(object):

    def setup_method(self, method):

        self.ref_taskyaml = '/path/to/foo/footask.yml'
        self.ref_input = {'file': 'foo.py', 'callable': 'bar', 'kwarg1': 'baz'}
        self.ref_env = {'task': self.ref_taskyaml}
        self.ref_pyfile = os.path.join(os.path.dirname(self.ref_taskyaml),
                                       self.ref_input['file'])

        self.stub_checkfile = Dingus('checkfile')
        self.stub_imported = Dingus('imported')
        self.stub_pyload = Dingus('pyload', dostuff__returns=('name',
                                                        self.stub_imported))
        self.stub_execute = Dingus('execute')

    def test_process_checks_file(self, monkeypatch):


        test_directive = python_directive.PythonDirective()
        monkeypatch.setattr(test_directive, 'checkfile', self.stub_checkfile)
        monkeypatch.setattr(test_directive, 'load_pyfile',
                            self.stub_pyload.dostuff)
        monkeypatch.setattr(test_directive, 'execute', self.stub_execute)

        test_directive.process(copy.copy(self.ref_input),
                                             self.ref_env)

        checkfile_call = self.stub_checkfile.calls[0]

        assert (checkfile_call.name, checkfile_call.args,
                checkfile_call.kwargs) == ('()', (self.ref_pyfile,), {})

    def test_process_loads_file(self, monkeypatch):

        test_directive = python_directive.PythonDirective()
        monkeypatch.setattr(test_directive, 'checkfile', self.stub_checkfile)
        monkeypatch.setattr(test_directive, 'load_pyfile',
                            self.stub_pyload.dostuff)
        monkeypatch.setattr(test_directive, 'execute', self.stub_execute)

        test_directive.process(copy.copy(self.ref_input),
                                             self.ref_env)

        pyload_call = self.stub_pyload.calls[0]

        assert (pyload_call.name, pyload_call.args, pyload_call.kwargs) == (
            'dostuff', (self.ref_pyfile,), {})

    def test_process_executes(self, monkeypatch):

        ref_kwargs = {'kwarg1': 'baz'}

        test_directive = python_directive.PythonDirective()
        monkeypatch.setattr(test_directive, 'checkfile', self.stub_checkfile)
        monkeypatch.setattr(test_directive, 'load_pyfile',
                            self.stub_pyload.dostuff)
        monkeypatch.setattr(test_directive, 'execute', self.stub_execute)

        test_directive.process(copy.copy(self.ref_input),
                                             self.ref_env)

        execute_call = self.stub_execute.calls[0]

        assert (execute_call.name, execute_call.args, execute_call.kwargs) == (
         '()', (self.stub_imported, self.ref_input['callable'], ref_kwargs), {})
