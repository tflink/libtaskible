============
libtaskotron
============

libtaskotron is a library for running automated tasks as part of the Taskotron_
system. The initial objective is (but is not limited to) automating selected
package checks in Fedora.

The project is not yet to be considered stable - the features contained are
generally functional but we will likely continue to make major changes in how
libtaskotron works.

For more information and API documentation, please look at `Taskotron project
page`__.

Please direct questions and comments to either *#fedora-qa* on freenode or the
`qa-devel mailing list <https://admin.fedoraproject.org/mailman/listinfo/qa-devel>`_.

.. _Taskotron: https://taskotron.fedoraproject.org
__ Taskotron_


Installing a Development Environment
====================================

Please consider whether you really need a libtaskotron development
environment. Maybe you simply want to develop tests *using* libtaskotron? In
that case, please follow `libtaskotron install instructions
<https://docs.qa.fedoraproject.org/libtaskotron/latest/docs/#install-libtaskotron>`_
instead. If you really want to develop libtaskotron itself, please continue.

For the moment, libtaskotron can't be fully installed by either pip or rpm and
needs a bit of both for now.

On your Fedora system, install the necessary packages::

  sudo dnf install       \
  createrepo             \
  gcc                    \
  git                    \
  koji                   \
  libtaskotron-config    \
  mash                   \
  python-doit            \
  python-hawkey          \
  python-pip             \
  python-rpmfluff        \
  python-virtualenv      \
  rpm-build              \
  rpm-python             \
  testcloud

If you have not yet cloned the repository, do it now::

  git clone https://pagure.io/taskotron/libtaskotron.git
  cd libtaskotron

Then, set up the virtualenv::

  virtualenv --system-site-packages env_taskotron
  source env_taskotron/bin/activate
  pip install -r requirements.txt

Finally, you should install libtaskotron, so that you don't need to specify the
full path for the taskotron runner every time you want to run it. You can either
use a standard installation, if you want to just use this project::

  pip install .

Or, if you intend to work on this project, you can install it in the editable
mode. This way you don't need to reinstall the project every time you make some
changes to it, the code changes are reflected immediately::

  pip install -e .

Before running any task, you also need to manually create a few required
directories. First, create a ``taskotron`` group if you don't have it already,
and add your user to it (you'll need to re-login afterwards)::

  getent group taskotron || sudo groupadd taskotron
  sudo usermod -aG taskotron <user>

Now create the directories with proper permissions::

  sudo install -d -m 775 -g taskotron /var/tmp/taskotron /var/log/taskotron \
    /var/cache/taskotron /var/lib/taskotron /var/lib/taskotron/artifacts \
    /var/lib/taskotron/images


Configuration
=============

The ``libtaskotron-config`` package installs config files with default values
into ``/etc/taskotron``. If you need to change those default values, you can
either change the files in ``/etc/taskotron`` or you can create config files
inside your checkout with::

  cp conf/taskotron.yaml.example conf/taskotron.yaml
  cp conf/yumrepoinfo.conf.example conf/yumrepoinfo.conf

The configuration files in ``conf/`` take precedence over anything in ``/etc``,
so make sure that you're editing the correct file if you create local copies.

In the development environment, it's also useful to have taskotron-generated
files automatically cleaned up, so that they don't occupy disk space in vain.
There is a tmpfiles.d template prepared for you, look into ``conf/tmpfiles.d``.


Running a Task
==============

A relatively simple example task is `rpmlint <https://bitbucket.org/fedoraqa/task-rpmlint>`_.

The task requires the rpmlint tool to be installed, so be sure to run::

  sudo dnf install rpmlint

To run that task against a koji build with NEVR ``<nevr>`` for some arch
``<arch>``, do the following::

  git clone https://bitbucket.org/fedoraqa/task-rpmlint.git
  runtask -i <nevr> -t koji_build -a <arch> task-rpmlint/runtask.yml

This will download the ``<nevr>`` from koji into a temp directory under
``/var/tmp/taskotron/``, run rpmlint on the downloaded rpms and print output in
YAML format to stdout.

Example::

  runtask -i htop-2.0.2-1.fc24 -t koji_build -a x86_64 task-rpmlint/runtask.yml


Using Disposable Clients for Task Execution
===========================================

When executing the task on the local machine is not desirable, you can use the
disposable client feature - a new virtual machine is spawned from a base image
and the task is executed in the VM.

Note: you need to setup Testcloud first refer to `Setting up Testcloud`_

To use the feature, just add ``--libvirt`` parameter to the runtask command::

  runtask --libvirt -i htop-2.0.2-1.fc24 -t koji_build -a x86_64 task-rpmlint/runtask.yml

By default, a base image defined by the ``imageurl`` option in ``taskotron.yaml``
config file is used. You can provide your own base image by changing the
``imageurl`` value (use ``file://`` URL to point to a local file).

We also provide updated base images at https://tflink.fedorapeople.org/taskotron/taskotron-cloud/images/
(note that the image needs to be uncompressed before use).

If you store these images in ``/var/lib/taskotron/images``, adhere to their
naming conventions and set ``force_imageurl=False`` in ``taskotron.yaml``,
we will find the latest one available automatically for you and you don't need
to update the ``imageurl`` option regularly.


Setting up Testcloud
--------------------

Configure ssh key for Testcloud to use (the private key must be passwordless).
Edit ``~/.config/testcloud/settings.py`` or ``/etc/testcloud/settings.py`` and
insert the respective public key in the place of the ``$SSH PUBKEY HERE$``
string::

  USER_DATA = """#cloud-config
  users:
    - default
    - name: root
      password: %s
      chpasswd: { expire: False }
      ssh-authorized-keys:
        - $SSH PUBKEY HERE$
  """


Running the Test Suite
======================

You can run the included test suite of unit and functional tests.
From the root checkout directory, execute::

  py.test testing/

to run unit tests, or::

  py.test --functional testing/

to run both unit and functional tests (recommended). Make sure to use the
virtualenv-provided version of py.test, not the system one, otherwise you'll
encounter import errors.

You can also see a test coverage of the code, just run::

  py.test --functional --cov-report=term-missing --cov=libtaskotron testing/

A nice HTML-based representation is available if you use ``--cov-report=html``
parameter instead.

If you write new tests, be sure to run this to see whether the code is
sufficiently covered by your tests.


Building Documentation
======================

Libtaskotron's documentation is written in `reStructuredText
<http://docutils.sourceforge.net/rst.html>`_ and built using `Sphinx
<http://sphinx-doc.org/>`_.

The documentation is easy to build if you have followed the instructions to set
up a development environment.

To actually build the documentation::

  doit builddocs


Build Automation
================

There are several development related tasks which are at least somewhat
automated using `doit <http://pydoit.org/>`_.

After either installing the doit package (``python-doit``, ``python3-doit``) or
via pip (``pip install doit``), you can see a list of available tasks and a short
description of those tasks by running ``doit list``. Some of the available tasks
are:

  * ``buildsrpm`` takes a snapshot of current git repo and uses the in-repo spec
    file to build a matching srpm in the ``builds/<version>`` directory. Note
    that if a snapshot already exists for a given version, a new snapshot will
    not be generated until the existing one is deleted.

  * ``chainbuild`` uses mockchain and the existing COPR repo to build a noarch
    binary rpm from the latest srpm.

  * ``builddocs`` builds documentation from current git sources

By default, the tool is pretty quiet but if you would like to see more verbose
output, add ``--verbosity 2`` to the doit command and all stdout/stderr output
will be shown.
