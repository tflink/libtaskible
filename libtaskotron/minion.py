# -*- coding: utf-8 -*-
# Copyright 2009-2015, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

from __future__ import absolute_import
import os.path
import yaml
import pipes

from libtaskotron import logger
from libtaskotron import config
from libtaskotron import remote_exec
from libtaskotron import python_utils
from libtaskotron.logger import log
from libtaskotron import taskformula
import libtaskotron.exceptions as exc

try:
    from libtaskotron.ext.disposable import vm
except ImportError, e:
    raise exc.TaskotronImportError(e)


class BaseMinion(object):
    '''Base Minion class that shouldn't be used on its own, it solely initiates inner attributes
    and environment. It also provides method _run() that delegates the task execution to Executor
    over SSH.

    :cvar tuple arg_data_exclude: a tuple of cmdline options/arguments which should not be
        forwarded from the host to the minion when execution the task. Use the same names as the
        stored variables in ``arg_data`` use (i.e. dashes converted to underscores, etc).
    :ivar dict formula: parsed task formula
    :ivar dict arg_data: processed cli arguments with some extra runtime variables
    :ivar str artifactsdir: path to ``artifactsdir`` for storing logs
    :ivar int exitcode: exit code of libtaskotron runner executed on the minion
    :ivar ssh: an instance of :class:`.ParamikoWrapper` which is used to execute all remote
        commands on a minion. ``None`` by default, you need to set this before you start
        :meth:`execute`.
    '''

    arg_data_exclude = ('task', 'libvirt', 'ssh', 'ssh_privkey', 'no_destroy', 'patch')

    def __init__(self, formula, arg_data):
        self.formula = formula
        self.arg_data = arg_data
        self.artifactsdir = self.arg_data['artifactsdir']

        self.user = arg_data.get('user', None)
        self.machine = arg_data.get('machine', None)
        self.port = arg_data.get('port', None)
        self.ssh_privkey = arg_data['ssh_privkey'] or config.get_config().ssh_privkey

        self.exitcode = None
        self.ssh = None

    def _prepare_task(self):
        '''Prepare the environment to run the task.'''
        # TODO: add sudo support

        log.info('Examining the minion and installing libtaskotron...')
        self.taskdir = config.get_config().client_taskdir

        # create initial DNF cache (but only if needed)
        # uses the same logic as in libtaskotron.ext.fedora.rpm_utils.dnf_cache_available()
        self.ssh.cmd('dnf --cacheonly repolist || dnf makecache')

        # add additional repos to minion
        repos = ['--add-repo %s' % pipes.quote(repo) for repo in config.get_config().minion_repos]
        if repos:
            self.ssh.cmd('dnf config-manager %s' % ' '.join(repos))

        # create DNF cache for taskotron repos
        # this hard-coding is very suboptimal, but it's hard to find a better way how to refresh
        # only taskotron repos (unless we require their names listed in configuration)
        excluded = ['fedora', 'updates', 'updates-testing']
        exclude_cmd = ' '.join(['--disablerepo %s' % pipes.quote(repo) for repo in excluded])
        self.ssh.cmd('dnf makecache %s' % exclude_cmd)

        # install libtaskotron
        # FIXME this should only need libtaskotron-core T651. libtaskotron-fedora should be
        # installed as a check dep.
        # TODO: remove tar dep once libtaskotron 0.4.16 has been released for long enough
        # (tar is here just for backward compatibility, see D965)
        self.ssh.install_pkgs(['libtaskotron-core', 'libtaskotron-fedora', 'tar'])

        log.info('Copying task files onto the minion...')

        # Copy config files. Go through CONF_DIRS in reversed order to preserve priority.
        for config_dir in reversed(config.CONF_DIRS):
            if os.path.exists(config_dir):
                self.ssh.put_dir(config_dir, '/etc/taskotron/')

        # create taskdir, remove the old one first
        self.ssh.cmd('rm -rf {0} && mkdir -p {0}'.format(pipes.quote(self.taskdir)))

        # patch remote libtaskotron if needed (for dev, shouldn't be used in production)
        if self.arg_data['patch'] is not None:
            self.ssh.put_file(os.path.abspath(self.arg_data['patch']), '%s/%s' % (
                self.taskdir, self.arg_data['patch']))
            self.ssh.install_pkgs(['patch'])
            self.ssh.cmd('patch -d /usr/lib/python2.7/site-packages/ -p1 -i %s/%s' % (
                pipes.quote(self.taskdir), pipes.quote(self.arg_data['patch'])))

        # put files needed for execution (task, input files, etc)
        self.ssh.put_dir(os.path.dirname(os.path.abspath(self.arg_data['task'])), self.taskdir)

        # need to have default_flow_style false to get valid yaml w/ nested dicts
        self.ssh.write_file(os.path.join(self.taskdir,
                                         os.path.basename(self.arg_data['task'])),
                            yaml.dump(self.formula, default_flow_style=False))

    def _run(self):
        '''The main method that runs the task'''

        task = os.path.basename(self.arg_data['task'])
        cmdline = python_utils.reverse_argparse(self.arg_data['_orig_args'],
                                                ignore=self.arg_data_exclude)
        cmdline.append('--local')
        cmdline.append(task)

        # be paranoid and escape everything
        cmdline = [pipes.quote(elem) for elem in cmdline]

        # execute task
        log.info('Executing the task on the minion...')
        task_cmd = ['cd', pipes.quote(self.taskdir), '&&', 'runtask'] + cmdline

        self.exitcode = self.ssh.cmd(' '.join(task_cmd))
        log.info('Task execution on the minion is complete')

    def _get_output(self):
        '''copy artifacts from vm'''
        log.info('Copying task artifacts and logs from the minion...')
        try:
            self.ssh.get_dir(self.artifactsdir, self.artifactsdir)
        except exc.TaskotronRemoteError, e:
            log.error(e)

    def execute(self):
        '''This method has to be implemented by classes that inherits from :class:`.BaseMinion`.
        It should contain environment initialization (namely set :attr:`ssh` to properly
        instantiated :class:`.ParamikoWrapper`) and cleanup (if necessary). Moreover, the order of
        private methods has to be: :meth:`_prepare_task`, :meth:`_run` and :meth:`_get_output`.
        '''
        raise exc.TaskotronNotImplementedError()


class PersistentMinion(BaseMinion):
    '''Minion class that connects to alredy running machine using SSH and runs the task there.
    '''

    def execute(self):
        '''Init environment and connect to remote machine for task execution.'''

        filelog_path = os.path.join(self.artifactsdir, 'taskotron-overlord.log')
        logger.add_filehandler(filelog_path=filelog_path, remove_mem_handler=True)

        stdio_filename = os.path.join(self.artifactsdir, 'taskotron-stdio.log')

        log.info("Running task over SSH on machine '%s'...", self.machine)

        with remote_exec.ParamikoWrapper(self.machine, self.port, self.user,
                                         self.ssh_privkey, stdio_filename) as self.ssh:
            try:
                self._prepare_task()
                self._run()
            except exc.TaskotronRemoteTimeoutError:
                # do not try to get logs - the minion is likely dead and we would hang on
                # getting the logs, see https://phab.qa.fedoraproject.org/T665
                log.error('Connection to the minion timed out')
                raise
            except exc.TaskotronRemoteError:
                # this is the case when remote execution failed but it should be possible to
                # get logs from the minion, so try it
                log.error('Error occurred on the minion, task execution stopped')
                self._get_output()
                raise
            else:
                self._get_output()


class DisposableMinion(BaseMinion):
    '''Minion class that creates a disposable client, connects to it over SSH and runs the task
    there.
    '''

    def execute(self):
        '''Init environment and connect to disposable client for task execution.'''

        filelog_path = os.path.join(self.artifactsdir, 'taskotron-overlord.log')
        logger.add_filehandler(filelog_path=filelog_path, remove_mem_handler=True)

        stdio_filename = os.path.join(self.artifactsdir, 'taskotron-stdio.log')

        execution_ok = False
        try:
            log.info("Running task on a disposable client using libvirt...")

            log.info("Creating VM for task")

            env = taskformula.devise_environment(self.formula, self.arg_data)

            task_vm = vm.TestCloudMachine(self.arg_data['uuid'])
            task_vm.prepare(**env)
            self.machine = task_vm.ipaddr
            self.port = 22
            self.user = task_vm.username

            log.info('Running task over SSH on machine %s (%s@%s:%s)', task_vm.instancename,
                     self.user, self.machine, self.port)

            task_vm.wait_for_port(self.port)

            with remote_exec.ParamikoWrapper(self.machine, self.port, self.user,
                                             self.ssh_privkey, stdio_filename) as self.ssh:
                try:
                    self._prepare_task()
                    self._run()
                except exc.TaskotronRemoteTimeoutError:
                    # do not try to get logs - the minion is likely dead and we would hang on
                    # getting the logs, see https://phab.qa.fedoraproject.org/T665
                    log.error('Connection to the minion timed out')
                    raise
                except exc.TaskotronRemoteError:
                    # this is the case when remote execution failed but it should be possible to
                    # get logs from the minion, so try it
                    log.error('Error occurred on the minion, task execution stopped')
                    self._get_output()
                    raise
                else:
                    self._get_output()

            execution_ok = True
        finally:
            if not execution_ok:
                log.warn('Task execution was interrupted by an error, doing emergency cleanup')

            if self.arg_data['no_destroy']:
                log.info('--no-destroy mode was set from command line, not tearing VM '
                         'down for task %s: %s@%s:%s', self.arg_data['uuid'], self.user,
                         self.machine, self.port)
            else:
                try:
                    log.info('Shutting down the minion...')
                    task_vm.teardown()
                # Catch 'em all since we can't be sure that every exception trown
                # is TaskotronRemoteError. If we omitted any exception that would
                # be later thrown in the teardown, we would suppress (effectively
                # make it silent) any possible exception from execution above
                # which is more important.
                except Exception, e:
                    if execution_ok:
                        raise e
                    else:
                        # a different exception is already being raised, don't override it
                        log.exception(e)
