# -*- coding: utf-8 -*-
# Copyright 2009-2015, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

from __future__ import absolute_import
import logging
import os.path
import argparse
import datetime
import sys
import copy

import libtaskotron
from libtaskotron import logger
from libtaskotron import config
from libtaskotron.logger import log
from libtaskotron.overlord import Overlord


# The list of accepted item types on the command line (--type option)
_ITEM_TYPES = [
    "bodhi_id",
    "koji_build",
    "koji_tag",
    "compose",
    "docker_image",
    "dist_git_commit",
]


def get_argparser():
    '''Get the cmdline parser for the main runner.

    :rtype: :class:`argparse.ArgumentParser`
    '''
    # IMPORTANT: When you add a new argument, look into `minion.BaseMinion.arg_data_exclude` and
    # update it if needed!

    parser = argparse.ArgumentParser()
    parser.add_argument("task", help="task formula to run")
    parser.add_argument("-a", "--arch",
                        choices=["i386", "x86_64", "armhfp", "noarch"],
                        action='append',
                        help="architecture specifying the item to be checked. If omitted, "
                             "defaults to noarch.")
    parser.add_argument("-i", "--item", help="item to be checked")
    parser.add_argument("-t", "--type",
                        choices=_ITEM_TYPES,
                        help="type of --item argument")
    parser.add_argument("-j", "--jobid", default="-1",
                        help="optional job identifier used to render log urls")
    parser.add_argument("-d", "--debug", action="store_true",
                        help="Enable debug output "
                             "(set logging level to 'DEBUG')")
    parser.add_argument("--uuid", default=datetime.datetime.utcnow().strftime("%Y%m%d_%H%M%S_%f"),
                        help="Unique job identifier for the execution"
                             "status tracking purposes. If unset, defaults to"
                             "current datetime in UTC")
    parser.add_argument("--override", action="append", default=[], metavar='VAR=VALUE',
                        help="override internal variable values used in runner "
                             "and the task formula. Value itself is evaluated "
                             "by eval(). This option can be used multiple times. "
                             "Example: --override \"workdir='/some/dir/'\"")
    parser.add_argument("-p", "--patch", help="patch to apply to remote system"
                                              "before execution of task")
    parser.add_argument("--local", action="store_true",
                        help="make the task run locally on this very machine (the default "
                             "behavior for development profile). This also approves any required "
                             "system-wide changes to be performed (automatic installation of "
                             "package dependencies, destructive tasks allowed, etc).")
    parser.add_argument('--libvirt', action='store_true',
                        help="make the task run remotely in a disposable client spawned using "
                             "libvirt (the default behavior for production profile).")
    parser.add_argument("--ssh", metavar='user@machine[:port]',
                        help="make the task run on a remote machine over ssh")
    parser.add_argument("--ssh-privkey", metavar="</path/to/private.key>",
                        help="path to private key for remote connections over ssh")
    parser.add_argument("--no-destroy", action="store_true",
                        help="do not destroy disposable client at the end of task execution")

    return parser


def check_args(parser, args):
    """ Check if passed args doesn't have conflicts and have proper format. In case of error, this
    function prints error message and exits the program.

    :param argparse.ArgumentParser parser: parser object used to show error message and exit the
                                           program
    :param dict args: arguments previously returned by argument parser converted to dict
    """

    if len([arg for arg in [args['local'], args['libvirt'], args['ssh']] if arg]) > 1:
        parser.error('Options --local, --libvirt and --ssh are mutually exclusive')

    if args['ssh']:
        if '@' not in args['ssh']:
            parser.error("SSH connection info not in format 'user@machine' or 'user@machine:port'")


def process_args(raw_args):
    """ Processes raw input args and converts them into specific data types that
    can be used by tasks. This includes e.g. creating new args based on
    (item, item_type) pairs, or adjusting selected architecture.

    :param dict raw_args: dictionary of raw arguments. Will not be modified.
    :returns: dict of args with appended/modified data
    """
    # do not modify the input dict
    args = copy.deepcopy(raw_args)

    # store the original unprocessed args for later use when passing it to the minion
    args['_orig_args'] = raw_args

    # process item + type
    if args['type'] in _ITEM_TYPES:
        args[args['type']] = args['item']

    # process arch
    if args['arch'] is None:
        args['arch'] = ['noarch']

    override = {}
    for var in args['override']:
        name, value = var.split('=', 1)
        override[name] = value
    args['override'] = override

    # parse ssh
    if args['ssh']:
        args['user'], machine = args['ssh'].split('@')
        if ':' in machine:
            args['machine'], port = machine.split(':')
            args['port'] = int(port)
        else:
            args['machine'] = machine
            args['port'] = 22

    # set paths
    args['artifactsdir'] = os.path.join(config.get_config().artifactsdir, args['uuid'])

    return args


def main():
    '''Main entry point executed by runtask script'''
    # Preliminary initialization of logging, so all messages before regular
    # initialization can be logged to stream.
    logger.init_prior_config()

    log.info('Execution started at: %s',
             datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S UTC'))
    log.debug('Using libtaskotron %s', libtaskotron.__version__)

    # parse cmdline
    parser = get_argparser()
    args = parser.parse_args()

    check_args(parser, vars(args))
    log.debug('Parsed arguments: %s', args)
    arg_data = process_args(vars(args))

    # initialize logging
    level_stream = logging.DEBUG if args.debug else None
    logger.init(level_stream=level_stream)

    # start execution
    overlord = Overlord(arg_data)
    overlord.start()

    # finalize
    log.info('Execution finished at: %s. Task artifacts were saved in: %s',
             datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S UTC'),
             arg_data['artifactsdir'])
    log.info("Exiting with exit status %d" % overlord.exitcode)
    sys.exit(overlord.exitcode)
