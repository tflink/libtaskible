# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

'''A wrapper object for yumrepoinfo.conf to access its information easily'''

from __future__ import absolute_import
import ConfigParser
import os

from libtaskotron import config
from libtaskotron.logger import log
from libtaskotron import arch_utils
from libtaskotron import exceptions as exc


# a singleton instance of YumRepoInfo (a dict with arches as keys)
_yumrepoinfo = {}


def get_yumrepoinfo(arch=None, filelist=None):
    '''Get YumRepoInfo instance. This method is implemented using the singleton
    pattern - you will always receive the same instance, which will get
    auto-initialized on the first method call.

    :param str arch: architecture to return the YumRepoInfo for. It's always
                     converted to basearch. If ``None``, then local machine arch
                     is used.
    :param filelist: list of config files to read information from. The
                     first available config file is used. If ``None``, then
                     the default list of locations is used.
    :return: shared :class:`YumRepoInfo` instance
    :raise TaskotronConfigError: if file config parsing and handling failed
    '''
    # converts to basearch, returns local arch for None
    arch = arch_utils.basearch(arch)

    if not arch in _yumrepoinfo:
        _yumrepoinfo[arch] = YumRepoInfo(arch, filelist)
    return _yumrepoinfo[arch]


class YumRepoInfo(object):
    '''This class is a wrapper for easily accessing repoinfo.conf file.'''

    def __init__(self, arch=None, filelist=None):
        '''
        :param str arch: architecture for which to adjust repo URLs. By default
                    it refers to the architecture of the current machine. It's
                    always converted to basearch.
        :param filelist: list of config files to read information from. The
                        first available config file is used. If ``None``, then
                        the default list of locations is used.
        :type filelist: iterable of str
        :raise TaskotronConfigError: if no YUM repositories data is found (empty
                                     or non-existent config file). It's not
                                     raised if you specifically request no data
                                     to load (``filelist=[]``).
        '''
        if (config.get_config().profile == config.ProfileName.TESTING and
            filelist is None):
            # Under the testing profile we don't want to try to load any config
            # files, they might not be available at all (T163). The unit test
            # can provide its own with `filelist` - in that case, load it.
            return
        self.arch = arch_utils.basearch(arch)
        self.filelist = (filelist if filelist is not None else
            [os.path.join(confdir, 'yumrepoinfo.conf')
             for confdir in config.CONF_DIRS])
        self.parser = ConfigParser.SafeConfigParser(defaults=
            {'arch': self.arch})

        if not self.filelist:
            # no data should be loaded
            return

        self._read()

        if not self.repos():
            msg = ("No YUM repo definitions found in the following locations: "
                   "%s" % self.filelist)
            log.critical(msg)
            raise exc.TaskotronConfigError(msg)


    def repos(self):
        '''Get the list of all known repository names.

        :rtype: list of str
        '''
        return self.parser.sections()


    def releases(self):
        '''Get the list of stable (supported) Fedora releases.

        :rtype: list of str
        '''
        return [r for r in self.repos() if self.get(r, 'release_status').lower()
                == "stable"]


    def branched(self):
        '''Get branched Fedora release (or None if it doesn't exist).

        :rtype: str or None
        '''
        for r in self.repos():
            if self.get(r, 'release_status').lower() == 'branched':
                return r
        return None


    def arches(self, reponame):
        '''Get the list of all supported architectures for a repo

        :param str reponame: repository name
        '''
        return self._getlist(reponame, 'arches')


    def get(self, reponame, key):
        '''Get a specific key value from a repo

        :param str reponame: repository name
        :param str key: name of the key you want to retrieve from a repository
                        (section)
        :raise TaskotronConfigError: if the key can't be retrieved (e.g. wrong
                                     key or repo name)
        '''
        try:
            return self.parser.get(reponame, key)
        except ConfigParser.Error as e:
            raise exc.TaskotronConfigError("Can't retrieve key '%s' from repo "
                "'%s': %s" % (key, reponame, e))


    def repo(self, reponame):
        '''Given a repo name, return the yumrepoinfo dict with keys:
        ``arches``, ``parents``, ``tag``, ``url``, ``path`` and ``name``

        :param str reponame: repository name
        :rtype: dict
        '''
        repo = {'arches': self.arches(reponame),
                'parent': self.get(reponame, 'parent'),
                'tag': self.get(reponame,'tag'),
                'url': self.get(reponame,'url'),
                'path': self.get(reponame,'path'),
                'name': reponame,
        }
        return repo


    def repo_by_tag(self, tag):
        '''Given a Koji tag, return the corresponding repo dict.

        :param str tag: a koji tag, e.g. ``f20-updates``. Note: ``rawhide`` is
                        not used as a Koji tag, use number identifier instead or
                        use :meth:`repo('rawhide')['tag'] <repo>` to discover it
                        first.
        :return: repo dict as from :meth:`repo`, or ``None`` if no such Koji tag
                 is found
        '''
        # we don't have repo definition for -candidate tags,
        # let's go up in the hierarchy (stripping '-candidate')
        if tag.endswith('-candidate'):
            tag = tag[:-len('-candidate')]
        for r in self.repos():
            if self.get(r,'tag') == tag:
                return self.repo(r)


    def top_parent(self, reponame):
        '''Go through the repo hiearchy and find the top parent for a repo

        :param str reponame: repository name
        :return: the top parent reponame. If ``reponame`` doesn't have any
                 parent, its name is returned (it's its own top parent)
        :rtype: str
        :raise TaskotronConfigError: if infinite parent loop detected
        '''
        parents = []
        repo = reponame

        while True:
            parent = self.get(repo, 'parent')

            if not parent:
                # we've found the top parent
                return repo

            # detect infinite parent loops
            if parent in parents:
                raise exc.TaskotronConfigError('Infinite parent loop detected '
                    'in yumrepoinfo: %s' % parents)

            parents.append(parent)
            repo = parent


    def release_status(self, reponame):
        '''Return release status of specified repo. For non-top-parent repos,
        return release_status of top parent repo.

        :param str reponame: repository name
        :return: release status of specified repo, lowercased. One of:
            `rawhide`, `branched`, `stable`, `obsolete`.
        :rtype: str
        '''
        return self.get(self.top_parent(reponame), 'release_status').lower()


    def _read(self):
        '''Read first available config file from the list of provided config
        files.'''
        log.debug('Looking for yumrepoinfo config files in: %s', self.filelist)

        for cfg in self.filelist:
            if self.parser.read(cfg):
                log.debug('Successfully loaded yumrepoinfo config file: %s',
                          cfg)
                break
            else:
                log.debug('Failed to load yumrepoinfo config file: %s', cfg)


    def _getlist(self, reponame, key):
        '''Transform a list of comma separated values into a Python list.
        Returns empty list for a non-existent key.

        :param str reponame: repository name
        :param str key: name of the key you want to retrieve from a repository
                        (section), and then convert it to a list
        :rtype: list of str
        '''
        itemlist = self.get(reponame, key)
        if not itemlist:
            return []
        else:
            return [t.strip() for t in itemlist.split(',')]
