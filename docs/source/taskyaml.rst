.. _taskotron-formula-format:

=============================
Taskotron Task Formula Format
=============================

One of the core bits of Taskotron is the formula yaml file used to describe
tasks to be run. As Taskotron is still a very young project, some components
of the task formulae are not yet used by the task runner but are useful to human
readers of the task. Those sections will be noted and those notes updated as
things change.

The documentation here is a description of how things are implemented but are
a little light on the practical creation of tasks. :ref:`writing-tasks-for-taskotron`
and some existing tasks are also good references:

  * `rpmlint <https://bitbucket.org/fedoraqa/task-rpmlint>`_
  * `examplebodhi <https://bitbucket.org/fedoraqa/task-examplebodhi>`_

Task Description
================

Metadata
--------

Some metadata is required to be in the task formula:

  * task name
  * description
  * maintainer

.. code-block:: yaml

  ---
  name: rpmlint
  desc: download specified rpms and run rpmlint on them
  maintainer: tflink

.. note::

  Please use a FAS account name for the maintainer


Input
-----

Most tasks require some form of input. The required input must be declared in
order to verify that the task can be run

.. code-block:: yaml

  input:
      args:
          - arch
          - koji_build

Valid args include:

  * arch (e.g.: ``x86_64``, ``[i386, x86_64]``)
  * koji_build (e.g.: ``xchat-0:2.8.8-19.fc19``, ``xchat-2.8.8-19.fc19``)
  * bodhi_id (e.g.: ``FEDORA-2013-10476``, ``xchat-2.8.8-19.fc19``)
  * koji_tag (e.g.: ``f20-updates-testing-pending``)

Dependencies
------------

A task may also require the presence of other code to support execution. Those
dependencies are specified as part of the environment description. Anything that
``dnf install`` supports as an argument on the command line is supported.

.. code-block:: yaml

  environment:
      rpm:
          - python-solv
          - python-librepo

.. note::

  Future versions will also support specifying dependencies in other formats
  than RPM.

Environment
-----------

.. note::

  This is only applicable when running in the disposable-minion mode, which
  is the default on our deployment.

By default, runtime environment (distro, release, arch and flavor) is devised
from the item under test for Koji builds and Bodhi updates. This affects the
base-image used to spawn the new virtual machine in the disposable-minion mode.

When the environment can not be devised, either because of unsupported
``item_type`` or unrecognized value of the ``item``, default environment (defined
in configuration by the ``default_disposable_{distro, release, arch, flavor}``
options) is used.

For example, ``runtask.py -i xchatex-2.8.8-21.fc20 -t koji_build -a x86_64 rpmlint.yml``
would use x86_64 (arch) Fedora (distro) 20 (release).

On top of that, environment requirements can be specified in the Task Formula,
although we do not think that these should be broadly used, there might be
a valid reason to, for example, aways run a task on x86_64 disregarding the item
under test.

All of the distro, release, arch and flavor can be defined in the environment
section of the task formula, and take precedence over the auto-devised values.

To specify environment in the formula use the ``environment`` section like
this:

.. code-block:: yaml

  environment:
      distro: fedora
      release: 24
      arch: x86_64

.. note::

  The task execution fails, when there is no base image fitting the
  distro/release/arch/flavor requirements.

Task Execution
==============

Every task is defined in the ``actions`` block of the formula yaml file. The task
consists of one or more actions which consist of a name, directive and optional
export of data.

.. code-block:: yaml

  actions:
      - name: using directivename for something
        directivename:
            arg1: value1
            arg2: ${some_variable}
        export: somestep_output


Variable Storage and Syntax
---------------------------

The formula yaml file uses modified `string.Template`_ syntax for variables. As in
the standard, you can use ``$variable`` or ``${variable}`` format, and you need to
make the dollar sign doubled if you want to include it literally (``$$not_a_variable``).

On top of that, you can use a dot (``.``) to access attributes of a variable.
For the sake of convenience, ``${foo.bar}`` does the following things on the Python layer:

  * check for an item ``bar`` in ``foo`` (``foo.__getitem__('bar')``)
  * if there is not, check for an attribute ``bar`` in ``foo`` (``getattr(foo, 'bar')``)
  * if there is not, raises ``TaskotronYamlError``

.. code-block:: yaml

  actions:
      - name: first step
        directive_one:
            arg1: value1
        export: firststep_output

      - name: second step
        directive_two:
            arg1: ${firststep_output.bar}

Variables can be created during task execution or provided by default by the
task runner.

.. _string.Template: https://docs.python.org/2.7/library/string.html?highlight=string#template-strings

Provided Variables
------------------

The task runner provides the following variables without a need for them to be
explicitly specified:

  * ``artifactsdir`` contains a path to the artifacts directory. ``artifactsdir``
    is used by tasks to store their various outputs like logs, archives,
    images or any other output. Artifacts, the content of ``artifactsdir``,
    are then exposed and accessible via http, e.g.
    https://taskotron.fedoraproject.org/artifacts/20150423/0a93d8fa-ea09-11e4-9d4b-525400062113/.
    See also uuid_.
  * ``checkname``, not suprisingly, contains a task name of the task currently
    being run by the runner. The reason why it's not called taskname rather
    than ``checkname`` is that there were changes in Taskotron terminology
    in the past and the name persisted. Before 3rd party task support is
    introduced, the terminology will be unified.
  * ``jobid`` is used primarially for reporting to render proper log and job
    urls. It can be specified on the command line but isn't required for local
    execution. If a ``jobid`` is not explicitly provided at execution time, a
    sane default value will be provided by the runner.

    .. _`uuid`:
  * ``uuid`` is an identification of a run which is unique across all parts of
    Taskotron. It can be specified on the command line. If a ``uuid`` is not
    explicitly provided at execution time, a
    sane default value will be provided by the runner.
  * ``workdir`` is often used by :ref:`taskotron-directives` which store files
    that serve as inputs for the task about to be run by the runner. ``workdir``
    also can be used for temporary files that the task might need during its
    execution.

Created Variables
-----------------

There are many situations in which the output from one step is used as input
for another step which is executed later. In order for a step's output to be
used later, it must first be exported.

.. code-block:: yaml

  actions:
      - name: first step
        directive_one:
            arg1: value1
        export: firststep_output

      - name: second step
        directive_two:
            arg1: ${firststep_output}

In this example, the output from ``first step`` is stored as ``firststep_output``
and later used by ``second step``. Once the ``first step`` is executed, its
output is stored as a variable using the name following ``export:``.


.. _taskotron-directives:

Directives
==========

The directives which are used in task steps are defined in libtaskotron. These
directives are grouped into three types: preparation, execution and reporting.
Below is a list of the current directives and what they can help you accomplish.
This is not the complete documentation for each of these directives, but it
should provide a good starting point for understanding what they do.

.. note::

  This directive documentation is incomplete and is meant to be temporary until
  we resolve `some issues <https://phab.qa.fedoraproject.org/T142>`_
  around generating documenation for the directives. For now, please see the
  source or ask questions about directives directly.

bodhi_comment
-------------

This is an interface to `bodhi <https://admin.fedoraproject.org/updates/>`_
comments. Allows the posting of comments on a specific update as well as karma
for the update.

bodhi
-----

This directive is used to download specific packages from
`bodhi <https://admin.fedoraproject.org/updates/>`_.

createrepo
----------

Takes a path from yaml input and creates a repository on that path.

dummy
-----

Primarily a testing directive, it simply returns a status and
optionally a message. Also works as a useful placeholder while
writing new tasks.

koji
----

Provides an interface to `koji <http://koji.fedoraproject.org/koji/>`_ for
downloading rpms with a specific NEVR or rpms with a specific tag.

mash
----

Creates repositories from locally downloaded rpms.

python
------

Allows the execution of a python script as part of task execution.
Offers several methods of executing python code for a task.

shell
-----

Allows the execution of a shell command(s) in subprocess call.

resultsdb
---------

Provides an interface to `resultsdb <https://pagure.io/taskotron/resultsdb>`_
for storing results of executed tasks.
