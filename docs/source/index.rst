==========================
Libtaskotron Documentation
==========================

What is libtaskotron?
=====================

Libtaskotron is one of the core components which make up the `Taskotron`_
system. Libtaskotron is responsible for running tasks written in
:ref:`taskotron-formula-format`.

While libtaskotron was designed for use with `Fedora <http://fedoraproject.org>`_,
the Fedora specific parts are isolated and the core should be usable with any
recent Linux distribution.

Libtaskotron and Taskotron should be considered Alpha software. They are
currently under very heavy development and will likely change until we stabilize
the interfaces.

Contents
--------

.. toctree::
   :maxdepth: 1

   quickstart
   writingtasks
   taskyaml
   library
   devguide
   directives/list_of_directives_modules
   resultyaml
   disposableclients
   distgittasks
   docker



.. _what-can-i-do-with-taskotron:

What Can I Do With Taskotron?
=============================

In theory, there is little that cannot be done in Taskotron; by design, it is
a system and framework for running semi-arbitrary tasks. That being said, there
is a limit to what we currently support. If there is some feature or language
that you'd like to see added to Taskotron, let us know through :ref:`contact-us`
or filing a feature request (:ref:`taskotron-bugs`).

Task Languages
--------------

Currently, the only language supported for tasks is Python. However, Taskotron
is designed to work on text-based interactions between the runner and other
units of work. Once the runner matures more, support for additional languages
may be added.

Directives
----------

A Taskotron task is made up of :ref:`taskotron-directives` which control what
can be done in a task. See the list of available :ref:`taskotron-directives`
to learn more about what is currently available.


.. _install-libtaskotron:

Installing libtaskotron
=======================

Refer to :doc:`quickstart` guide.


.. _libtaskotron-modules:

libtaskotron modules
--------------------

As functionality has been added to libtaskotron, we've started breaking up that
functionality into modules to increase flexibility and shrink the minimal
install profile. Each module is packaged separately for easy installation.

libtaskotron is composed of 3 modules - ``libtaskotron-core``,
``libtaskotron-fedora`` and ``libtaskotron-disposable``.


**libtaskotron-core**
  The minimum needed to run tasks with libtaskotron. It consists of the most
  basic bits that most tasks will use.

**libtaskotron-fedora**
  The fedora-specific functionality which is in libtaskotron - things like bodhi,
  koji and rpm-based directives.

**libtaskotron-disposable**
  The bits needed to enable execution with `disposable clients <disposableclients.html>`_. Local-only execution
  is still possible without this module but it was separated off to reduce
  the minimal installation profile when the functionality is not needed.

  Note that ``libtaskotron-disposable`` depends on `testcloud
  <https://github.com/Rorosha/testcloud>`_ which will pull in several other
  non-trivial dependencies.


.. _running-tasks:

Running Tasks
=============

Using `rpmlint <https://bitbucket.org/fedoraqa/task-rpmlint>`_ as an example,
the task repository contains the following important files::

  task-rpmlint/
      rpmlint.py
      runtask.yml

Rpmlint runs off of koji builds, so to run it against the x86_64 build of
``foo-1.2-3.fc99`` (substitute a real build if you're going to run this)::

  runtask -i foo-1.2-3.fc99 -t koji_build -a x86_64 runtask.yml

The build will be downloaded to a local tempdir, rpmlint will be run against
that build and the results will be printed to the console in :ref:`resultyaml-format`.

Using Remote Machines
----------------------

While the default execution path will run tasks on a local machine, it is also
possible to delegate task execution to a remote machine or spawn a disposable
virtual machine for task execution.

To use a remote machine, use the ``--ssh <user>@<ip address>[:port]`` argument
to runtask. This will signal the executor to connect to the indicated machine
and execute tasks there instead of locally.

To use a disposable client vm, use ``--libvirt`` but note that the image
available by default is very generic and this can add significantly to
execution time.


.. _contact-us:

Contact Us
==========
Please direct questions and comments to either the `Fedora QA Devel List
<https://admin.fedoraproject.org/mailman/listinfo/qa-devel>`_ or the *#fedora-qa*
IRC channel on `Freenode <http://freenode.net/>`_.



.. _Taskotron: https://fedoraproject.org/wiki/Taskotron


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
