#! /usr/bin/env python
# encoding: utf-8

import os
import codecs
import re
import subprocess
from datetime import datetime

from doit.tools import create_folder
from doit import get_var


################################################################################
# PROJECT DEFINITIONS
# This dodo file is designed to be pretty much identical in all qadevel
# projects. This section should be the only part which differs between them.
################################################################################

NAME = 'libtaskotron'
MOCKENV = 'fedora-23-x86_64'
TARGETDIST = 'fc23'
BUILDARCH = 'noarch'
COPRREPOS = ['https://copr-be.cloud.fedoraproject.org/results/tflink/taskotron/fedora-23-x86_64/']
# virtualenv name to use for operations which require it
VENVNAME = 'env_testing'

# These are deps needed in a virtualenv which are not on pypi
VENV_GIT_REPOS = ['https://pagure.io/taskotron/resultsdb_api.git',
                  'https://github.com/Rorosha/testcloud.git']

################################################################################
# UTILITY FUNCTIONS
# Functions used to generate some of the needed metadata
################################################################################

def get_version():
    """ Assume that the version is stored as __version__ in $APPNAME/__init__.py
    so that it can be generically extracted
    """

    version_file = codecs.open(os.path.join(NAME, '__init__.py'), 'r').read()
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")


def get_rpmrelease(specfile):
    """parse the rpm release out of the specfile that we're currently using"""

    if os.system('which rpmspec &>/dev/null') != 0:
        raise RuntimeError('rpmspec was not found in the system, '
                           'run "dnf install rpm-build" to install it')

    rpmspec_command = ['rpmspec', '-q', '--queryformat="%{RELEASE}\\n"', specfile]
    raw_output = subprocess.check_output(rpmspec_command).split()[0].lstrip('"')
    return raw_output.split('.')[0]


def listdirs(targetdir):
    """List all dirs in a targetdir, separated out as dirs and symlinks"""
    output_contents = [os.path.join(targetdir, d) for d in os.listdir(targetdir)]

    dirs = [d for d in output_contents if os.path.isdir(d) and not os.path.islink(d)]

    links = [d for d in output_contents if os.path.islink(d)]

    return {'dirs': dirs, 'links': links}


def get_gitbranch():
    """retrieve the current git branch"""

    gitbranch_command = ['git', 'rev-parse', '--abbrev-ref', 'HEAD']
    raw_output = subprocess.check_output(gitbranch_command)
    return raw_output.strip()


def maybe_use_virtualenv(commands):
    """Adds activate and deactivate commands to a chain of otherwise normal
    bash commands if the ENVTYPE variable is set to 'ci'. Note that this is
    making assumptions about what's installed as packages and you may need
    to install a few more things in the virtualenv (testcloud, sphinx etc.)"""

    if ENVTYPE != 'ci':
        return commands

    real_commands = []
    for command in commands:
        real_commands.append('source {}/bin/activate; {}'.format(VENVNAME, command))

    return real_commands

################################################################################
# GENERAL VARIABLES
# These variables are computed based on project definitions and current state
# as a convenience for later functions
################################################################################

VERSION = get_version()
TARGETNAME = "{}-{}".format(NAME, VERSION)
SPECFILE = '{}.spec'.format(NAME)
RPMRELEASE = get_rpmrelease(os.path.abspath(SPECFILE))
NVR = '{}-{}-{}.{}'.format(NAME, VERSION, RPMRELEASE, TARGETDIST)
SHATYPE = 'sha256'

HERE = os.path.abspath(os.path.dirname(__file__))

ENVTYPE = get_var('envtype', 'dev')

CURBRANCH = get_gitbranch()

# many branches have slashes in them, make sure they'll be one dir
SAFEBRANCH = CURBRANCH.replace('/', '_')

BASEDIR = get_var('basedir', os.path.join(HERE, 'builds'))



OUTDIR = os.path.join(BASEDIR, VERSION)
RELEASES_OUTDIR = OUTDIR
DOCS_OUTDIR = OUTDIR
BUILD_OUTDIR = OUTDIR

# We want to be doing things a little differently when we're in CI mode
if ENVTYPE == 'ci':
    outversion = VERSION

    # if we're on master, use the tag name
    if CURBRANCH.upper() != 'MASTER':
        outversion = SAFEBRANCH

    RELEASES_OUTDIR = os.path.join(BASEDIR, 'releases', NAME, outversion)
    DOCS_OUTDIR = os.path.join(BASEDIR, 'docs', NAME, outversion)


################################################################################
# PROJECT TASKS
# These tasks should be pretty much consistent for all qadevel projects, driven
# by the variables listed above
################################################################################

def task_chainbuild():
    """mockchain package using COPR repo to assist. Will fail if deps in repo are not up to date"""
    tmpdir_prefix = '{}-{}'.format(NAME, datetime.utcnow().strftime('%Y%m%d%H%M%S'))
    mockchain_command = 'mockchain -r {} --tmp_prefix={} -a {} '\
                        '{}/{}.src.rpm'.format(MOCKENV, tmpdir_prefix,
                                               ' -a '.join(COPRREPOS),
                                               RELEASES_OUTDIR, NVR)
    cp_command = 'cp /var/tmp/mock-chain-{}*/results/{}/{}/*.{}.rpm '\
                 '{}/.'.format(tmpdir_prefix, MOCKENV, NVR, BUILDARCH,
                               RELEASES_OUTDIR)

    return {'actions': [mockchain_command, cp_command],
            'task_dep': ['buildsrpm'],
            'targets': ['{}.rpm'.format(NVR)]
            }


def task_buildsrpm():
    """build SRPM using mock, the latest git shapshot and current specfile details"""
    mocksrpm_command = '/usr/bin/mock -r {} --buildsrpm --spec={} '\
                       '--sources={}'.format(MOCKENV, SPECFILE, RELEASES_OUTDIR)
    cp_command = 'cp /var/lib/mock/{}/result/{}-{}-{}.{}.src.rpm '\
                 '{}/.'.format(MOCKENV, NAME, VERSION, RPMRELEASE, TARGETDIST,
                               RELEASES_OUTDIR)

    return {'actions': [mocksrpm_command, cp_command],
            'task_dep': ['snapshot'],
            'targets': ['{}.src.rpm'.format(NVR)]
            }


def task_snapshot():
    """Take snapshot of git specified git branch"""
    archive_command = 'git archive {} --prefix={}/ | gzip -c9 > '\
                      '{}.tar.gz'.format(CURBRANCH,
                                         TARGETNAME,
                                         os.path.join(RELEASES_OUTDIR, TARGETNAME))

    return {'actions': [(create_folder, [RELEASES_OUTDIR]),
                        archive_command],
            'targets': ['{}.tar.gz'.format(TARGETNAME)],
            }


def task__checksum():
    """Generate checksum of git snapshot"""
    sha_command = '{shatype}sum {outdir}/{target}.tar.gz > '\
                  '{outdir}/{target}.tar.gz.{shatype}'.format(shatype=SHATYPE,
                                                              target=TARGETNAME,
                                                              outdir=RELEASES_OUTDIR)
    return {'actions': [sha_command],
            'task_dep': ['snapshot'],
            'targets': ['{}.tar.gz.{}'.format(TARGETNAME, SHATYPE)]
            }


def task_builddocs():
    """Generate sphinx html docs"""
    return {'actions': maybe_use_virtualenv(['pushd docs;make html']),
            'task_dep': ['_checkvirtualenv'],
            'targets': ['docs/build/html']
            }


def task_cleandocs():
    """Clean generated sphinx html docs"""
    return {'actions': ['pushd docs;make clean'],
            }


def task_releasedocs():
    """Put generated docs in the release output"""
    return {'actions': ['mkdir -p {}'.format(DOCS_OUTDIR),
                        'cp -r docs/build/html/* {}'.format(DOCS_OUTDIR)],
            'task_dep': ['builddocs'],
            'targets': ['{}/docs'.format(DOCS_OUTDIR)]
            }


def set_symlink(basedir, source, linkdest):
    """make relative symlink between source and linkdest in basedir"""

    fullsource = os.path.join(basedir, source)
    fulldest = os.path.join(basedir, linkdest)

    # check if the source exists
    if not os.path.exists(fullsource):
        raise Exception("Attempted to create symlink to non existant \
                        directory: {}".format(fullsource))

    # check if the symlink already exists
    if os.path.exists(fulldest):
        # remove the existing link so we can re-create it
        print("Removing old latest symlink")
        os.remove(fulldest)

    cwd = os.getcwd()
    os.chdir(basedir)
    os.symlink(source, linkdest)
    os.chdir(cwd)


def task_updatelatest():
    """Make sure that the 'latest' symlink in output directory points to the correct version"""

    def updatelatest():
        # this isn't needed for non-release branches
        if CURBRANCH.upper() != 'MASTER':
            return

        if not os.path.exists(DOCS_OUTDIR):
            raise Exception("DOCS_OUTDIR ({}) does not exist, cannot fix ",
                            "symlinks".format(DOCS_OUTDIR))
        else:
            set_symlink(os.path.split(DOCS_OUTDIR)[0], VERSION, 'latest')

        if not os.path.exists(RELEASES_OUTDIR):
            raise Exception("RELEASES_OUTDIR ({}) does not exist, cannot fix ",
                            "symlinks".format(RELEASES_OUTDIR))
        else:
            set_symlink(os.path.split(RELEASES_OUTDIR)[0], VERSION, 'latest')


    return {'actions': [updatelatest],
            'targets': [],
            'task_dep': ['releasedocs'],
            }


def task_test():
    """Run the unit and functional tests"""
    return {'actions': maybe_use_virtualenv(['py.test -F testing/']),
            'task_dep': ['_checkvirtualenv']
            }


def task__checkvirtualenv():
    """Check for properly named virtualenv, if it doesn't exist, create it"""

    def get_venv_commands():
        if ENVTYPE != 'ci':
            return []
        else:
            activate = '{}/bin/activate'.format(VENVNAME)
            commands = ['virtualenv --system-site-packages {}'.format(VENVNAME)]

            for repo in VENV_GIT_REPOS:
                commands.append('. {}; pip install git+{}'.format(activate, repo))

            commands.append('. {}; pip install --force-reinstall -r '
                            'requirements.txt'.format(activate))

        return commands

    return {'actions': get_venv_commands(),
            'targets': [VENVNAME],
            }


def task__details():
    """ Display some calculated variables used in the script for debug purposes"""

    return {'actions': ['echo "taskotron version: {}"'.format(VERSION),
                        'echo "target name: {}"'.format(TARGETNAME),
                        'echo "output dir: {}"'.format(OUTDIR)],
            'verbosity': 2
            }
